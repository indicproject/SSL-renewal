#!/bin/bash

set -e

CURRENT_DATE=$(date +"%Y-%m-%d-%H-%M-%S")
CERT_FILE=${CURRENT_DATE}/fullchain.cer
KEY_FILE=${CURRENT_DATE}/${DOMAIN}.key

echo "Generating Certificates"
/root/.acme.sh/acme.sh --issue --dns dns_dgon -d "${DOMAIN}" -d "*.${DOMAIN}" --yes-I-know-dns-manual-mode-enough-go-ahead-please --server letsencrypt

echo "Moving certificates to dated folder ${CURRENT_DATE}"
mkdir "${CURRENT_DATE}"
cp -vr ~/.acme.sh/indicproject.org/* "${CURRENT_DATE}"

#echo "Copying certificates to server"
#scp -r "${CURRENT_DATE}" "${SERVER_ACCESS}":~/.certificates/
#scp -r "${CURRENT_DATE}"/* "${SERVER_ACCESS}":~/.certificates/latest/

echo "Updating GitLab Pages certificates - indicproject.org"
curl -s \
     -X PUT \
     -H "PRIVATE-TOKEN: ${GITLAB_PAT}" \
     --form "certificate=@${CERT_FILE}" \
     --form "key=@${KEY_FILE}" \
     "https://gitlab.com/api/v4/projects/${WEBSITE_PROJECT_ID}/pages/domains/${DOMAIN}"
